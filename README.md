# IndexedDB_REST

Use indexeddb like api rest.

##Install
```
npm -i indexddb-rest
```


##Setup

We use this library like indexeddb, so we need make a litle setup.

note: All created tables have id automatically.

```js
let ref = require('indexddb-rest');

const IndexedDBREST = ref.IndexedDBREST;

function onError(err) {

}

let db = new IndexedDBREST( 'myDB', 1, [{name: 'table1'}, {name: 'table2'}], onError);

```

##Create data

When you add a value to a table, the id is automatically incremented.
Return a copy of the item created.

```js
db.post('table1',{some: 'nanananana'}).then(response => {
    console.log('save something', response);
}).catch(error => {
    console.log('bad something happened');
});
```
If you dont need generate a row with autoid, use this.
```js
db.post('table1',{some: 'nanananana'}, false).then(response => {
    console.log('save something', response);
}).catch(error => {
    console.log('bad something happened');
});
```

##Read data

We have two methods: get and list.

Get return one element of the table.

List return all elememts of the table.

Return a copy of the item solicited.
```js
db.get('table1', 142563652).then(response => {
    console.log('the item', response);
}).catch(error => {
    console.log('not exist the item');
});
```

Return a copy of the list.
```js
db.list('table1').then(response => {
    console.log('the list', response);
}).catch(error => {
    console.log('');
});
```

##Update data
Note: if the id no exist, the item is save automatically.
Return a copy of the item updated.
```js
db.put('table1',{id: 142563652, some: 'nanananana'}).then(response => {
    console.log('upddate something', response);
});
```
 
##Delete data

All items have a id, so when we delete a item use the id of item for delete it.

Return true or false.
```js
db.delete('table1', 142563652).then(response => {
    console.log('delete the item', response);
});
```