export class IndexedDBREST {

    isStart: boolean;
    db: any;
    tablas: any[] = [];
    folio: number;
    onError: Function;
    /**
     * Constructor
     * @param {*} nameDB Nombre de la base de datos
     * @param {*} versionDB 
     * @param {*} onError callback que regresa TODOS los errores de ejecución
     */
    constructor(
        nameDB: string,
        versionDB: number,
        tablas: any[],
        onError: Function
    ) {
        this.isStart = false;
        this.db=null;
        this.onError = onError;
        let obj=this;
        let indexedDB = window.indexedDB ;
        let open = indexedDB.open(nameDB, versionDB);
        this.tablas = tablas;
        this.folio = Date.now();
        //si no existe la base o se actualiza la version se crea todo el schema
        open.onupgradeneeded = () => {
            let db = open.result;
            if (tablas){
                for (const tabla of tablas){
                    db.createObjectStore(tabla, { keyPath: 'id' });
                }
            }
            obj.db = open.result;
        };
        open.onsuccess = function(){
            obj.db = open.result;            
            obj.isStart = true;
        }
        open.onerror = (e) => {
            if (!obj.isStart) {
                obj.isStart = false;
            }
            if (onError) {
                onError(e);
            }
        }
    }

    getFolio() {
        this.folio++;
        return this.folio;
    }

    copy(json: any){
        return JSON.parse(JSON.stringify(json));
    }

    /**
     * Agrega un registro a la tabla _table
     * @param _table tabla con la que se trabajara
     * @param _datos datos a guardar
     * @return Una promesa que devuelve el registro agregado o el error
     */
    post(
        _table: string,
        _data: any,
        _auto_id: boolean = true
    ){
        let obj = this;
        return new Promise<any>(
            (resolve, reject) =>{
                if (!obj.db){
                    reject('Error: no init indexedDB');
                } else {
                    try {
                        let db    = obj.db;
                        let tx    = db.transaction(_table, 'readwrite');
                        let store = tx.objectStore(_table);
                        let json = _data;
                        if (_auto_id) {
                            json.id = obj.getFolio();
                        }
                        let request = store.put(json);
                        request.onsuccess = function() {
                            let out = _data;                        
                            resolve ( obj.copy(out) ) ;
                        };
                        request.onerror = function(e:any){
                            this.onError({type: 'POST', error: e});
                            reject('Ocurrio un error');
                        }
                        tx.oncomplete = function() {                            
                        };
                    } catch (error) {
                        setTimeout( () => {
                            obj.post(_table, _data).then( res => { resolve(res)})
                            .catch(err=>{reject(err)});
                        }, 1000);
                    }                    
                }
            }
        );
    }

    /**
     * Funcion busca un elemento en la tabla dada.
     * @param _table tabla con la que se trabajara
     * @param _key llave a buscar
     * @return una promesa que contienen el elemento encontrado o el error
     */
    get(
        _table: string,
        _key: number
    ){
        let obj = this;
        return new Promise<any>((resolve, reject) =>{
            if (!obj.db){
                reject('Error: no init indexedDB');
            } else {
                try {
                    let db    = obj.db;
                    let tx    = db.transaction(_table, 'readonly');
                    let store = tx.objectStore(_table);
                    let request=store.get(_key);
                    request.onsuccess=function(){
                        let out = request.result;                        
                        resolve ( obj.copy(out) ) ;
                    }
                    request.onerror = function(e:any) {
                        this.onError({type: 'GET', error: e});
                        resolve(null);
                    }
                    tx.oncomplete = function() {
                        
                    };    
                } catch (error) {
                    setTimeout( () => {
                        obj.get(_table, _key).then( res => { resolve(res)})
                        .catch(err=>{reject(err)});
                    }, 1000);
                }
                
            }
        });
    }
    
    /**
     * Esta funcion manda el error o la actualizacion hasta que se desocupa la transacion
     * @param _table Tabla que se actualizara
     * @param _data json con los cambios
     */
    put(
        _table: string,
        _data: any
    ){
        let obj = this;
        return new Promise<any>((resolve, reject) =>{
            if (!obj.db){
                reject('Error: no init indexedDB');
            } else {
                try {
                    let db    = obj.db;
                    let tx    = db.transaction(_table, 'readwrite');
                    let store = tx.objectStore(_table);
                    if (!_data.hasOwnProperty('id')) {
                        _data['id'] = obj.getFolio();
                    }
                    let request = store.put(_data);
                    request.onsuccess = function() {
                        let out = _data;                        
                        resolve ( obj.copy(out) ) ;
                    };
                    request.onerror = function(e:any){
                        this.onError({type: 'PUT', error: e});
                        reject('Ocurrio un error');
                    }
                    tx.oncomplete = function() {
                    };    
                } catch (error) {
                    setTimeout( () => {
                        obj.put(_table, _data).then( res => { resolve(res)})
                        .catch(err=>{reject(err)});
                    }, 1000);
                }
                
            }
        });
    }

    /**
     * Funcion que elimina un registro a la tabla _table
     * @param _table tabla con la que se trabajara
     * @param _key elemento a eliminar     
     * @return Una promesa que devuelve true si se elimina o false si no se pudo
     */
    delete(
        _table: string,
        _key: number
    ){
        let obj = this;
        return new Promise<any>((resolve, reject) =>{
            try {                    
                let db    = obj.db;
                let tx    = db.transaction(_table, 'readwrite');
                let store = tx.objectStore(_table);
                let requets=store.delete(_key);
                requets.onsuccess=function(e:any){
                    let out = true;                        
                    resolve(out);
                }
                requets.onerror = function(e:any) {
                    let out = false;                        
                    this.onError({type: 'DELETE', error: e});
                    resolve(out);
                }
                tx.oncomplete = function() {
                    
                };                      
            } catch (error) {
                setTimeout( () => {
                    obj.delete(_table, _key).then( res => { resolve(res)})
                    .catch(err=>{reject(err)});
                }, 1000);
            }
        });
    }

    /**
     * Funcion que lista los elementos de latabla dada
     * @param _table tabla con la que trabajara
     * @return una promesa que devuelve la lista de elementos de la tabla dada o el error
     */
    list(
        _table: string
    ){
        let obj = this;
        return new Promise<any>(
            (resolve, reject) =>{
                try {
                    let db    = obj.db;
                    let tx    = db.transaction(_table, 'readonly');
                    let store = tx.objectStore(_table);
                    let list=store.getAll();
                    list.onsuccess = function(){
                        let out = list.result;                           
                        resolve ( obj.copy(out) ) ;
                    }
                    list.onerror = function (e:any) {
                        this.onError({type: 'LIST', error: e});
                        reject(e);
                    }
                    tx.oncomplete = function() {
                        
                    };    
                } catch (error) {
                    setTimeout( () => {
                        obj.list(_table).then( res => { resolve(res)});
                    }, 1000);
                }
            }
        );
    }

    /**
     * Funcion para eliminar el contenido de una tabla
     * @_table_ nombre de la tabla a limpiar
     * @return devuelve una promesa que contienen true o el error.
     */
    clear(
        _table: string
    ){
        let obj = this;        
        return new Promise<any>(
            (resolve, reject) =>{
                if (!obj.db){
                    reject('Error: no init indexedDB');
                } else {
                    let db    = obj.db;
                    let tx    = db.transaction(_table, 'readwrite');
                    let store = tx.objectStore(_table);
                    let req = store.clear();
                    req.onsuccess = function(evt:any) {
                        let out = true;                        
                        resolve(out);
                    };
                    req.onerror = function(e:any) {
                        this.onError({type: 'CLEAR', error: e});
                        reject(e);
                    }
                }
            }
        );
    }
}
